# UniOnline - Facilitando sua vida EAD

Este é o código fonte do UniOnline, contendo todos os arquivos necessários para compilar e rodar o UniOnline.

---

O diretório `unionline-server` contém a API/Proxy para a comunicação com o servidor do Lyceum oficial da UniEvangélica. 

Já o diretório `unionline-frontend` contém o Frontend com a interface gráfica web feito com o *Vue CLI 3*.

---

Todas as contribuições são bem-vindas, porém comente o código em *inglês* e documente Wikis ou `README.md`s em *português*.