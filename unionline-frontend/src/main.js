import Vue from "vue";
import App from "./App.vue";
import router from "./router";
// import VueMoment from "vue-moment";

import VueCookie from "vue-cookie";

Vue.use(VueCookie);

// import "./registerServiceWorker";

import store from "./store.js";

import Vue2TouchEvents from "vue2-touch-events";
Vue.use(Vue2TouchEvents, {
  swipeTolerance: 80
});
// Vue.use(VueSweetalert2);
// Vue.use(VueMoment);

// console.log(VueMoment
// )

Vue.config.productionTip = false;

new Vue({
  store,
  router,
  render: function(h) {
    return h(App);
  }
}).$mount("#app");
