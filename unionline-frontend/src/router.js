import Vue from "vue";
import Vuex from "vuex";
import Router from "vue-router";

import Dash from "./views/Dash.vue";
import Login from "./views/Login.vue";

import Avisos from "./views/Avisos.vue";
import Horarios from "./views/Horarios.vue";
import Faltas from "./views/Faltas.vue";
import Notas from "./views/Notas.vue";
import Dadospessoais from "./views/Dadospessoais.vue";
import Boletos from "./views/Boletos.vue";

// import Loading from "./views/Loading.vue";

import store from "./store.js";

Vue.use(Router);
Vue.use(Vuex);

// TODO: Group lyceumroutes in a children route named "lyceum" like "/dashboard/lyceum/..."
// TODO: Update the Dashboards route detection to find this subroute

const lyceumroutes = [
  {
    name: "Avisos",
    path: "avisos",
    component: Avisos,
    // loading: Loading,
    meta: {
      icon: "mdi-bell",
      title: "Avisos"
    }
  },
  {
    name: "Horários",
    path: "horarios",
    component: Horarios,
    // loading: Loading,
    meta: {
      icon: "mdi-calendar-clock",
      title: "Horários"
    }
  },
  {
    name: "Faltas",
    path: "faltas",

    component: Faltas,
    // loading: Loading,
    meta: {
      icon: "mdi-calendar-alert",
      badge: 0,
      title: "Faltas"
    }
  },
  {
    name: "Notas",
    path: "notas",

    component: Notas,
    // loading: Loading,
    meta: {
      icon: "mdi-chart-bar",
      badge: 0,
      title: "Notas"
    }
  },
  {
    name: "Boletos",
    path: "boletos",

    component: Boletos,
    // loading: Loading,
    meta: {
      icon: "mdi-barcode",
      title: "Boletos"
    }
  },
  {
    name: "Dados pessoais",
    path: "dadospessoais",
    // loading: Loading,
    component: Dadospessoais,
    meta: {
      icon: "mdi-account-details",
      title: "Dados pessoais"
    }
  }
];

const router = new Router({
  // mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      redirect: "/login"
    },
    {
      path: "/dashboard",
      name: "dashboard",
      component: Dash,
      meta: {},
      beforeEnter: (to, from, next) => {
        // Check if user is authenticated
        // If this isn't checked, the dashboard would be empty
        // Also, be noted that if Vue Router can't access the store, it will always redirect back to the login screen
        const isUserLoggedIn = store.state.aluno.matricula > "";
        if (!isUserLoggedIn) {
          next("/login");
        } else {
          next();
        }
      },
      children: lyceumroutes
    },
    {
      path: "/login",
      name: "login",
      component: Login,
      meta: {
        title: "Login"
      },
      beforeEnter: (to, from, next) => {
        // Prevent going to the login page if logged in
        const isUserLoggedIn = store.state.aluno.matricula > "";
        if (isUserLoggedIn) {
          next(false);
        } else {
          next();
        }
      }
    }
  ]
});

router.beforeEach((to, from, next) => {
  if (store.state.modal_open) {
    next(false);
    store.dispatch("MODAL_OPEN_CHANGE", false); // "Close" the modal (dispatch a change that will reflect in the modal watcher)
  } else {
    next();
  }
  document.title = "UniOnline • " + to.meta.title;
});

export default router;
