import Vue from "vue";
import Vuex from "vuex";
import Axios from "axios";
import b64toBlob from "b64-to-blob";
import router from "./router";

Vue.use(Vuex);
export default new Vuex.Store({
  state: {
    avisos: [],
    publicacoes: [],
    horarios: [],
    faltas: [],
    notas: [],
    boletos: [],
    dadospessoais: {},
    aluno: {
      nome: "",
      matricula: "",
      curso: ""
    },
    modal_open: false
  },
  // Mutations are basically setters, which set some value to the Vuex state
  mutations: {
    STORE_AVISOS: (state, payload) => {
      state.avisos = payload;
    },
    STORE_PUBLICACOES: (state, payload) => {
      state.publicacoes = payload;
    },
    STORE_HORARIOS: (state, payload) => {
      state.horarios = payload;
    },
    STORE_FALTAS: (state, payload) => {
      state.faltas = payload;
    },
    STORE_NOTAS: (state, payload) => {
      state.notas = payload;
    },
    STORE_BOLETOS: (state, payload) => {
      state.boletos = payload;
    },
    STORE_USED_MATRICULA: (state, payload) => {
      state.aluno.nome = payload.aluno;
      state.aluno.matricula = payload.matricula;
      state.aluno.curso = payload.curso;
    },
    STORE_DADOSPESSOAIS: (state, payload) => {
      state.dadospessoais = payload;
    },
    SET_USER_EMPTY: (state, payload) => {
      state.aluno = {};
    },
    SET_MODAL_OPEN: (state, payload) => {
      state.modal_open = payload;
    }
  },
  // Actions are basically methods, which then commit (mutations/set) the Vuex store
  actions: {
    MODAL_OPEN_CHANGE: async (context, payload) => {
      // console.log("MODAL_OPEN_CHANGE", payload);

      context.commit("SET_MODAL_OPEN", payload);
    },
    LYCEUM_LOGIN: async (context, payload) => {
      let { data } = await Axios({
        method: "POST",
        url: "api/authentication/login",
        data: {
          username: payload.username,
          password: payload.password
        }
      });
      return data;
    },
    LYCEUM_LOGOUT: async context => {
      let { data } = await Axios({
        method: "GET",
        url: "api/authentication/logout"
      });
      context.commit("SET_USER_EMPTY");
      router.push("/login");
      return true;
    },
    LYCEUM_KEEPALIVE: async () => {
      let { data } = await Axios({
        method: "GET",
        url: "api/keepalive"
      });
      return true;
    },
    LYCEUM_GET_AVISOS: async (context, matricula) => {
      let { data } = await Axios({
        method: "GET",
        url: "api/avisos/get?matricula=" + matricula
      });
      // console.log("AVISOS", data.data);
      context.commit("STORE_AVISOS", data.data);
    },
    LYCEUM_GET_PUBLICACOES: async (context, matricula) => {
      let { data } = await Axios({
        method: "GET",
        url: "api/publicacoes/get?matricula=" + matricula
      });
      // console.log("PUBLICACOES", data.data);

      // Function is directly here, because it will only be used in this action
      // Sorts the publicacoes object by "date".
      // Newest comes first (Descending)
      function compare(a, b) {
        if (a.data < b.data) return 1;
        if (a.data > b.data) return -1;
        return 0;
      }
      data.data.sort(compare);

      context.commit("STORE_PUBLICACOES", data.data);
    },
    LYCEUM_GET_HORARIOS: async (context, matricula) => {
      let { data } = await Axios({
        method: "GET",
        url: "api/horarios?matricula=" + matricula
      });
      // console.log("HORARIOS", data.data);
      context.commit("STORE_HORARIOS", data.data);
    },
    LYCEUM_GET_FALTAS: async (context, matricula) => {
      let { data } = await Axios({
        method: "GET",
        url: "api/faltas?matricula=" + matricula + "&additional=true"
      });
      // console.log("FALTAS", data.data);
      // console.log("TOTALFALTAS",router.options.routes[1].children[2].meta.badge§
      const faltas_route = router.options.routes
        .filter(route => {
          return route.path == "/dashboard";
        })[0]
        .children.filter(childrenRoute => {
          return childrenRoute.path == "faltas";
        })[0].meta;

      const faltas = data.data;

      let total_faltas = 0;
      faltas.map(disciplina => {
        return (total_faltas += parseInt(disciplina.faltas_acumuladas));
      });

      faltas_route.badge = total_faltas;

      context.commit("STORE_FALTAS", faltas);
    },
    LYCEUM_GET_NOTAS: async (context, matricula) => {
      let { data } = await Axios({
        method: "GET",
        url: "api/notas?matricula=" + matricula
      });
      // console.log("NOTAS", data.data);
      // console.log("TOTALFALTAS",router.options.routes[1].children[2].meta.badge§
      // const faltas_route = router.options.routes
      //   .filter(route => {
      //     return route.path == "/dashboard";
      //   })[0]
      //   .children.filter(childrenRoute => {
      //     return childrenRoute.path == "notas";
      //   })[0].meta;

      // const faltas = data.data;

      // let total_faltas = 0;
      // faltas.map(disciplina => {
      //   return (total_faltas += parseInt(disciplina.faltas_acumuladas));
      // });

      // faltas_route.badge = total_faltas;

      context.commit("STORE_NOTAS", data.data);
    },
    LYCEUM_GET_DADOSPESSOAIS: async context => {
      let { data } = await Axios({
        method: "GET",
        url: "api/dadospessoais"
      });
      // console.log("DADOS PESSOAIS", data.data);
      context.commit("STORE_DADOSPESSOAIS", data.data);
    },
    LYCEUM_GET_BOLETOS: async (context, matricula) => {
      let { data } = await Axios({
        method: "GET",
        url: "api/boletos/get?matricula=" + matricula
      });
      // console.log("BOLETOS", data.data);

      const boletos_route = router.options.routes
        .filter(route => {
          return route.path == "/dashboard";
        })[0]
        .children.filter(childrenRoute => {
          return childrenRoute.path == "boletos";
        })[0].meta;

      const boletos = data.data;

      let total_boletos = 0;
      boletos.map(boleto => {
        return total_boletos++;
      });

      boletos_route.badge = total_boletos;

      context.commit("STORE_BOLETOS", boletos);
    },
    LYCEUM_DOWNLOAD_PUBLICACAO: async (context, payload) => {
      let { data } = await Axios({
        method: "GET",
        url: "api/publicacoes/download?file_id=" + payload.file_id,
        responseType: "arraybuffer"
      });

      var bytes = new Uint8Array(data);
      var blob = new Blob([bytes], { type: payload.enc_type });

      return blob;
    },
    LYCEUM_DOWNLOAD_BOLETO: async (context, payload) => {
      let { data } = await Axios({
        method: "GET",
        url: "api/boletos/download?matricula=" + payload.matricula + "&boleto_id=" + payload.boleto_id,
        responseType: "arraybuffer"
      });

      var bytes = new Uint8Array(data);
      var blob = new Blob([bytes]);

      return blob;
    },
    LYCEUM_DOWNLOAD_ANEXO_AVISO: async (context, payload) => {
      let { data } = await Axios({
        method: "GET",
        url: "api/avisos/download?aviso_id=" + payload.aviso_id + "&arquivo_id=" + payload.arquivo_id,
        responseType: "blob"
      });

      const blob = new Blob([data]);

      return blob;
    },
    LYCEUM_GET_MATRICULAS: async () => {
      let { data } = await Axios({
        method: "GET",
        url: "api/matriculas"
      });

      // console.log("MATRICULAS", data.data);

      // Function is directly here, because it will only be used in this action
      // Sorts the matricula object by "situacao". Ascending (from A to Z).
      // "Ativo" happens to be "A", "Cancelado" = "C" and "Transferido" = "T",
      // so that means that the most relevant will be always on top of the list
      function compare(a, b) {
        if (a.situacao < b.situacao) return -1;
        if (a.situacao > b.situacao) return 1;
        return 0;
      }
      data.data.sort(compare);

      return data.data;
    },
    LYCEUM_SET_USED_MATRICULA: async (context, matricula) => {
      // Receives a object containing the entire matricula data
      context.commit("STORE_USED_MATRICULA", matricula);
    }
  }
});
