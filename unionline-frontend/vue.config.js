process.env.VUE_APP_VERSION = require("./package.json").version;

module.exports = {
  // ...other vue-cli plugin options...
  // pwa: {
  // configure the workbox plugin
  // workboxPluginMode: "InjectManifest",
  // workboxOptions: {
  // swSrc is required in InjectManifest mode.
  // swSrc: "public/service-worker.js"
  // ...other Workbox options...
  // }
  // },
  // configureWebpack: {
  //   plugins: [
  //     new webpack.DefinePlugin({
  //       "process.env": {
  //         PACKAGE_JSON: '"' + escape(JSON.stringify(require("./package.json"))) + '"'
  //       }
  //     })
  //   ]
  // },
  devServer: {
    proxy: {
      "/api": {
        target: "http://localhost:5000",
        changeOrigin: true
      }
    }
  }
};
