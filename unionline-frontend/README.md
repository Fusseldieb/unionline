# UniOnline - Facilitando sua vida EAD

Este é o código fonte do UniOnline, contendo todos os arquivos necessários para compilar e rodar o *Frontend* do UniOnline.

Tenha em mente de que o UniOnline necessita um servidor/API para rodar corretamente. Durante o desenvolvimento, tenha a certeza de que está apontando para um endereço de *backend* válido, que esteja rodando o backend/API do UniOnline.

Todas as contribuições são bem-vindas, porém comente o código em *inglês* e documente Wikis ou `README.md`s em *português*.

#### Para baixar todas as dependências necessárias, digite:
```
npm install
```

#### Com o comando abaixo, você executará o UniOnline em modo de desenvolvedor
(No modo de desenvolvedor, alterações em arquivos farão a interface atualizar em tempo real. Isso também é chamado de *hot-reloading*)
```
npm run serve
```

#### Para compilar e minificar tudo, use:
(Use isso somente para "produção", ou seja, uma versão final. Esse comando disponibilizará todos os arquivos necessários na pasta `dist/` para rodar o Frontend do UniOnline)
```
npm run build
```