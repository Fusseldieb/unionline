const axios = require("axios");
const querystring = require("querystring"); // To parse the POST properly so the dinossaur-age server can read it
const clog = require("../consoleLogger.js");
const reason_msg = require("../reasonMessages.js");
const converters = require("../converters.js");

const faltas = require("./faltas.js");
const disciplinas = require("./disciplinas.js");

async function getNotas(session, notas_obj) {
  const resp = await axios({
    method: "POST",
    url: "/AOnline/avaliacao/T012D.ajax",
    data: querystring.stringify({
      _id: "gpfNotasDisciplina",
      _p_0: notas_obj.matricula,
      _p_1: notas_obj.disciplina_id,
      _p_2: notas_obj.turma_id,
      _p_3: notas_obj.ano_letivo,
      _p_4: notas_obj.semestre,
      _p_5: "" // Don't know if needed
    }),
    headers: {
      Cookie: `JSESSIONID=${session.JSESSIONID};`,
      cronos_xsrf_token: session.XSRFToken
    }
  });
  return resp;
}

const proxy_fns = {
  // The parameter "req" should contain the "req" element from Express,
  // containing the server response, session and other things.
  async get(req) {
    // TODO: Maybe _getFaltas. Faltas has the media and the disciplina name. Disciplinas hasn't.
    const resp = await faltas._getFaltas(req.session, req.query.matricula);

    const recv = resp.data;
    let sendArray = [];
    if (typeof recv == "object") {
      var discipl = recv.data.records;
      for (var i = 0; i < discipl.length; i++) {
        // From Lyceum
        // TODO: There are duplicate ids below. Just get one of each!
        // ["06981 - ANIMAÇÃO", "PRÁTICA", null, "60", "34", "15", "86.67", "8", "<img src=\"../../image/ico/btFalta.png\" title=\"Detalhes das Faltas\"/>", "06981", "0065-04A-PRÁTICA", "2019", "07/01/2019 00:00:00", "12/31/2019 00:00:00", "Matriculado", "2"],
        // ["06981 - ANIMAÇÃO", "TEORICA", null, "20", "34", "8.5", "76.47", "8", "<img src=\"../../image/ico/btFalta.png\" title=\"Detalhes das Faltas\"/>", "06981", "0065-04A-TEORICA", "2019", "07/01/2019 00:00:00", "12/31/2019 00:00:00", "Matriculado", "2"],
        // ["06082 - ATIVIDADES MULTIDISCIPLINARES II", null, null, "40", "30", "10", "85", "6", "<img src=\"../../image/ico/btFalta.png\" title=\"Detalhes das Faltas\"/>", "06082", "0065-04A", "2019", "07/01/2019 00:00:00", "12/31/2019 00:00:00", "Matriculado", "2"],
        // ["06982 - DESIGN DE EMBALAGEM", "PRÁTICA", null, "20", "36", "9", "77.78", "8", "<img src=\"../../image/ico/btFalta.png\" title=\"Detalhes das Faltas\"/>", "06982", "0065-04A-PRÁTICA", "2019", "07/01/2019 00:00:00", "12/31/2019 00:00:00", "Matriculado", "2"],
        // ["06982 - DESIGN DE EMBALAGEM", "TEORICA", null, "60", "32", "15", "83.33", "10", "<img src=\"../../image/ico/btFalta.png\" title=\"Detalhes das Faltas\"/>", "06982", "0065-04A-TEORICA", "2019", "07/01/2019 00:00:00", "12/31/2019 00:00:00", "Matriculado", "2"],
        // ["06983 - DESIGN EDITORIAL", "PRÁTICA", null, "40", "34", "10", "80", "8", "<img src=\"../../image/ico/btFalta.png\" title=\"Detalhes das Faltas\"/>", "06983", "0065-04A-PRÁTICA", "2019", "07/01/2019 00:00:00", "12/31/2019 00:00:00", "Matriculado", "2"],
        // ["06983 - DESIGN EDITORIAL", "TEORICA", null, "40", "34", "10", "80", "8", "<img src=\"../../image/ico/btFalta.png\" title=\"Detalhes das Faltas\"/>", "06983", "0065-04A-TEORICA", "2019", "07/01/2019 00:00:00", "12/31/2019 00:00:00", "Matriculado", "2"],
        // ["09519 - EMPREENDEDORISMO - EAD", null, null, "40", "0", "10", "100", "0", "<img src=\"../../image/ico/btFalta.png\" title=\"Detalhes das Faltas\"/>", "09519", "EAD - 20192", "2019", "07/01/2019 00:00:00", "12/31/2019 00:00:00", "Matriculado", "2"],
        // ["09527 - METODOLOGIA DO TRABALHO CIENTÍFICO - EAD", null, null, "40", "0", "10", "100", "0", "<img src=\"../../image/ico/btFalta.png\" title=\"Detalhes das Faltas\"/>", "09527", "EAD - 20192", "2019", "07/01/2019 00:00:00", "12/31/2019 00:00:00", "Matriculado", "2"],
        // ["06985 - PROJETO DE PESQUISA EM DESIGN", null, null, "80", "76", "20", "85", "12", "<img src=\"../../image/ico/btFalta.png\" title=\"Detalhes das Faltas\"/>", "06985", "0065-04A", "2019", "07/01/2019 00:00:00", "12/31/2019 00:00:00", "Matriculado", "2"],
        // ["07550 - SOFTWARE EM DESIGN IV", null, "78", "80", "80", "20", "80", "16", "<img src=\"../../image/ico/btFalta.png\" title=\"Detalhes das Faltas\"/>", "07550", "0065-04A", "2019", "07/01/2019 00:00:00", "12/31/2019 00:00:00", "Aprovado", "2"]
        // TODO: Things below won't work anymore. The above method was changed!

        const notas_res = await getNotas(req.session, {
          disciplina_id: discipl[i][9],
          turma_id: discipl[i][10],
          ano_letivo: discipl[i][11],
          semestre: discipl[i][15],
          matricula: req.query.matricula
        });

        const notas_id_recv = notas_res.data.data.records;

        notas_id_recv.forEach(element => {
          // [1] [ 'VA3',
          // [1]   '3ª VERIFICAÇÃO DE APRENDIZAGEM',
          // [1]   '-',
          // [1]   null,
          // [1]   null,
          // [1]   '100',
          // [1]   '50',
          // [1]   'Histograma de Notas',
          // [1]   'EAD - 20192',
          // [1]   '09519',
          // [1]   '2019',
          // [1]   '2' ]

          let duplicate_item = sendArray.filter(elem => {
            if (elem.disciplina_id == element[9]) {
              return elem;
            }
          })[0];
          if (duplicate_item) {
            duplicate_item.avaliacoes.push({
              avaliacao_curto: element[0],
              avaliacao_nome: converters.RemoveStarAtEnd(element[1]),
              nota_maxima: element[5],
              nota: element[6],
              nota_calculada: element[1].indexOf("*", element[1].length - 1) == -1 // If a star exists, it means that the grade wasn't calculated yet
            });
          } else {
            sendArray.push({
              disciplina_id: element[9],
              disciplina_nome: discipl[i][0],
              ano_letivo: element[10],
              semestre: element[11],
              media: discipl[i][2],
              avaliacoes: [
                {
                  avaliacao_curto: element[0],
                  avaliacao_nome: converters.RemoveStarAtEnd(element[1]),
                  nota_maxima: element[5],
                  nota: element[6],
                  nota_calculada: element[1].indexOf("*", element[1].length - 1) == -1
                }
              ]
            });
          }

          // sendArray.push({
          //   avaliacao_curto: element[0],
          //   avaliacao_nome: element[1],
          //   nota_maxima: element[5],
          //   nota: element[6],
          //   // disciplina: element[8], // This can be the same between different disciplines. Don't use it!
          //   disciplina_id: element[9], // This is the ID of the discipline
          //   ano_letivo: element[10],
          //   semestre: element[11]
          // });
        });
      }

      reason = reason_msg("request_general_ok");

      clog("debug", "Formatter done processing successfully");
    } else {
      console.log(resp.data);
      reason_msg("formatter_type_mismatch");
    }

    return {
      success: resp.status == 200,
      data: sendArray
    };
  }
};

module.exports = proxy_fns;
