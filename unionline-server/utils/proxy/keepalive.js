const axios = require("axios");
const querystring = require("querystring"); // To parse the POST properly so the dinossaur-age server can read it
const clog = require("../consoleLogger.js");
const reason_msg = require("../reasonMessages.js");
const converters = require("../converters.js");

async function keepalivePing(session) {
  const resp = await axios({
    method: "GET",
    url: "/AOnline/keepalive", // This route doesn't exist and throws an Apache Error. It's a placeholder only to keep the connection alive.
    headers: {
      Cookie: `JSESSIONID=${session.JSESSIONID};`,
      cronos_xsrf_token: session.XSRFToken
    }
  });
  return resp;
}

const keepalive = {
  // The parameter "req" should contain the "req" element from Express,
  // containing the server response, session and other things.
  async get(req) {
    const resp = await keepalivePing(req.session);

    // if (typeof resp.data == "object") {
      // const avisoscount = resp.data.data.count;
      // const publicacoes = resp.data.data.records;

      console.log(resp.data);

      // clog("debug", "publicacoes count: " + publicacoescount);

    //   var publicacoes_fetched = [];
    //   for (var i = 0; i < publicacoes.length; i++) {
    //     const disciplina = publicacoes[i][0];
    //     var nome_arquivo = publicacoes[i][1];
    //     var nome_arquivo_raw = publicacoes[i][2];
    //     var data = publicacoes[i][4];
    //     var arquivo_id = publicacoes[i][5];

    //     publicacoes_fetched.push({
    //       disciplina: converters.CapitalizeFirstEach(disciplina),
    //       nome_arquivo: nome_arquivo,
    //       nome_arquivo_raw: nome_arquivo_raw,
    //       data: converters.DateToTimestamp(data),
    //       arquivo_id: arquivo_id
    //     });
    //   }

    //   reason = reason_msg("request_general_ok");

    //   clog("debug", "Formatter done processing successfully");
    // } else {
    //   console.log(resp.data);
    //   reason_msg("formatter_type_mismatch");
    // }

    return {
      success: 200
    };
  }
};

module.exports = keepalive;
