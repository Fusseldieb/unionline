const axios = require("axios");
const querystring = require("querystring"); // To parse the POST properly so the dinossaur-age server can read it
const clog = require("../consoleLogger.js");
const reason_msg = require("../reasonMessages.js");
const converters = require("../converters.js");

// Fetchs all messages from the "main preview"
// They only contain a small part of their content (a preview)
// But all IDs to fetch each of them later
async function getSurfaceMsgs(session, matricula) {
  const resp = await axios({
    method: "POST",
    url: "/AOnline/avisos/T016D.ajax",
    data: querystring.stringify({
      _id: "grpAvisos",
      _p_0: matricula
    }),
    headers: {
      Cookie: `JSESSIONID=${session.JSESSIONID};`,
      cronos_xsrf_token: session.XSRFToken
    }
  });
  return resp;
}

// Return one message specifying a ID and a matricula
async function getOneMsg(session, aviso_id, matricula) {
  const resp = await axios({
    method: "POST",
    url: "/AOnline/avisos/T017D.ajax",
    data: querystring.stringify({
      _id: "rcpDadosOcorrencia",
      _p_0: matricula,
      _p_4: aviso_id // Aviso OK
    }),
    headers: {
      Cookie: `JSESSIONID=${session.JSESSIONID};`,
      cronos_xsrf_token: session.XSRFToken
    }
  });

  return resp.data.data.records[0][2];
}

async function getMsgAttachment(session, aviso_id) {
  const resp = await axios({
    method: "POST",
    url: "/AOnline/avisos/T017D.ajax",
    data: querystring.stringify({
      _id: "grpListaAvisoAnexos",
      _p_0: aviso_id
    }),
    headers: {
      Cookie: `JSESSIONID=${session.JSESSIONID};`,
      cronos_xsrf_token: session.XSRFToken
    }
  });
  return resp.data.data.records;
}

async function downloadMsgAttachment(session, message_id, file_id) {
  const resp = await axios({
    responseType: "arraybuffer",
    method: "POST",
    url: "/AOnline/avisos/T017D.ajax",
    data: querystring.stringify({
      // TODO: Debug needed here. Attachment was not found
      _id: "gpfBaixarAvisoAnexo",
      _p_0: message_id, // Aviso ID
      _p_1: file_id, // Arquivo ID
      _timestamp: 0 // Original: 1551521911114 - Probably not needed
    }),
    headers: {
      Cookie: `JSESSIONID=${session.JSESSIONID};`,
      cronos_xsrf_token: session.XSRFToken
    }
  });

  const buffer = Buffer.from(resp.data);

  return buffer;
}

const proxy_fns = {
  // The parameter "req" should contain the "req" element from Express,
  // containing the server response, session and other things.
  // This function gets _all_ full messages/"avisos" from a particular "matricula"
  async get(req) {
    const resp = await getSurfaceMsgs(req.session, req.query.matricula);

    if (typeof resp.data == "object") {
      const avisoscount = resp.data.data.count;
      const avisos = resp.data.data.records;

      clog("debug", "Avisos count: " + avisoscount);

      var avisos_fetched = [];
      for (var i = 0; i < avisos.length; i++) {
        const data = avisos[i][4];
        const aviso_id = avisos[i][10];
        const full_message = await getOneMsg(
          req.session,
          aviso_id,
          req.query.matricula
        );
        const anexos = await getMsgAttachment(req.session, aviso_id);
        const anexos_obj = anexos.map(item => {
          return {
            nome_arquivo_raw: item[0],
            tamanho: item[2],
            data: converters.DateToTimestamp(item[3]),
            arquivo_id: item[5]
          };
        });
        avisos_fetched.push({
          mensagem: full_message,
          data: converters.SQLDateToTimestamp(data),
          aviso_id: aviso_id,
          anexos: anexos_obj
        });
      }

      reason = reason_msg("request_general_ok");

      clog("debug", "Formatter done processing successfully");
    } else {
      console.log(resp.data);
      reason_msg("formatter_type_mismatch");
    }

    return {
      success: resp.status == 200,
      data: avisos_fetched
    };
  },

  async download(req) {
    console.log("DOWNLOAD", req.query.aviso_id, req.query.arquivo_id);
    const buffer = await downloadMsgAttachment(
      req.session,
      req.query.aviso_id,
      req.query.arquivo_id
    );
    return buffer;
  }
};

module.exports = proxy_fns;
