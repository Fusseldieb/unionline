const axios = require("axios");
const querystring = require("querystring"); // To parse the POST properly so the dinossaur-age server can read it
const clog = require("../consoleLogger.js");
const reason_msg = require("../reasonMessages.js");
const converters = require("../converters.js");

const horarios = require("./horarios.js");
const disciplinas = require("./disciplinas.js");

const proxy_fns = {
  async _getFaltas(session, matricula) {
    const resp = await axios({
      method: "POST",
      url: "/AOnline/avaliacao/T012D.ajax",
      data: querystring.stringify({
        _id: "grpDisciplinas",
        _p_0: matricula,
        _p_1: "" // Not sure if needed
      }),
      headers: {
        Cookie: `JSESSIONID=${session.JSESSIONID};`,
        cronos_xsrf_token: session.XSRFToken
      }
    });
    return resp;
  },

  // The parameter "req" should contain the "req" element from Express,
  // containing the server response, session and other things.
  async get(req) {
    const faltas_resp = await _getFaltas(req.session, req.query.matricula);

    const withAdditionalInfo = req.query.additional == "true";

    let recvHorariosArray = [];
    let recvDisciplinasArray = [];

    if (withAdditionalInfo) {
      // Sometimes the name of the professor is in horarios, but no in disciplinas, and vice versa
      // So fetch both

      const horarios_resp = await horarios._getHorarios(req.session, req.query.matricula);
      recvHorariosArray = horarios_resp.data.data.records;

      const disciplinas_resp = await disciplinas._getDisciplinas(req.session, req.query.matricula);
      recvDisciplinasArray = disciplinas_resp.data.data.records;
    }

    const recvFaltasArray = faltas_resp.data.data.records;
    let sendArray = [];

    for (let index = 0; index < recvFaltasArray.length; index++) {
      const disciplina_id = recvFaltasArray[index][0].split(" - ")[0];
      const disciplina = recvFaltasArray[index][0].split(" - ")[1];
      const media = recvFaltasArray[index][2]; // Don't know for sure if that's 'media de notas' yet. Testing...
      const aulas_previstas = recvFaltasArray[index][3];
      const aulas_ministradas = recvFaltasArray[index][4];
      const faltas_permitidas = parseInt(recvFaltasArray[index][5]);
      const percentual_presenca = recvFaltasArray[index][6];
      const faltas_acumuladas = parseInt(recvFaltasArray[index][7]);
      const turma_id = recvFaltasArray[index][10];
      const ano = recvFaltasArray[index][11];
      const disciplina_duracao_de = recvFaltasArray[index][12];
      const disciplina_duracao_ate = recvFaltasArray[index][13];
      const matriculado = recvFaltasArray[index][14];

      const horarios_arr = recvHorariosArray.filter(item => {
        return item[0].split(" - ")[0] == disciplina_id;
      })[0];

      const disciplinas_arr = recvDisciplinasArray.filter(item => {
        return item[7] == disciplina_id;
      })[0];

      let professor = "";
      if (horarios_arr && horarios_arr[4] != "null") {
        professor = horarios_arr[4];
      } else if (disciplinas_arr && disciplinas_arr[4] != "null") {
        professor = disciplinas_arr[4];
      }

      let duplicate_item = sendArray.filter(elem => {
        if (elem.disciplina_id == disciplina_id) {
          return elem;
        }
      })[0];
      if (duplicate_item) {
        // Duplicate discipline_id found. Adding "faltas" together.
        duplicate_item.faltas_acumuladas += faltas_acumuladas;
        duplicate_item.faltas_permitidas += faltas_permitidas;
      } else {
        sendArray.push({
          disciplina_id: disciplina_id,
          disciplina: converters.CapitalizeFirstEach(disciplina),
          media: media,
          aulas_previstas: aulas_previstas,
          aulas_ministradas: aulas_ministradas,
          faltas_permitidas: faltas_permitidas,
          percentual_presenca: percentual_presenca,
          faltas_acumuladas: faltas_acumuladas,
          turma_id: turma_id,
          ano: ano,
          disciplina_duracao_de: disciplina_duracao_de,
          disciplina_duracao_ate: disciplina_duracao_ate,
          matriculado: matriculado,
          professor: converters.CapitalizeFirstEach(professor)
        });
      }
    }

    return {
      success: faltas_resp.status == 200,
      data: sendArray
    };
  }
};

module.exports = proxy_fns;
