const axios = require("axios");
const querystring = require("querystring"); // To parse the POST properly so the dinossaur-age server can read it
const clog = require("../consoleLogger.js");
const reason_msg = require("../reasonMessages.js");
const converters = require("../converters.js");

const proxy_fns = {
  async _getHorarios(session, matricula) {
    const resp = await axios({
      method: "POST",
      url: "/AOnline/calendario/T007D.ajax",
      data: querystring.stringify({
        _id: "grpHorariosSemana",
        _p_0: matricula
      }),
      headers: {
        Cookie: `JSESSIONID=${session.JSESSIONID};`,
        cronos_xsrf_token: session.XSRFToken
      }
    });
    return resp;
  },

  // The parameter "req" should contain the "req" element from Express,
  // containing the server response, session and other things.
  async get(req) {
    const resp = await this._getHorarios(req.session, req.query.matricula);

    // This function formats the received data
    // in order to only deliver the necessary informations
    //const formatted = format_data(resp.data);

    const recv = resp.data;

    var recvObj = recv.data.records;
    var sendArray = [];
    for (let index = 0; index < recvObj.length; index++) {
      const disciplina_id = recvObj[index][0].split(" - ")[0];
      const disciplina = recvObj[index][0].split(" - ")[1];
      const turma_id = recvObj[index][1].split(" <br> ")[0];
      const turma_duracao_de = recvObj[index][1]
        .split(" <br> ")[1]
        .split(" a ")[0];
      const turma_duracao_ate = recvObj[index][1].split(" a ")[1];
      const local = recvObj[index][2].split(" <br/> ")[0];
      const sala = recvObj[index][2].split(" <br/> ")[1];
      const hora = recvObj[index][3].split("<br>")[0];
      const carga = recvObj[index][3].split("<br>")[1];
      const professor = recvObj[index][4];
      const dia_semana = recvObj[index][6];

      sendArray.push({
        disciplina_id: disciplina_id,
        disciplina: converters.CapitalizeFirstEach(disciplina),
        turma_id: turma_id,
        turma_duracao_de: turma_duracao_de,
        turma_duracao_ate: turma_duracao_ate,
        local: converters.CapitalizeFirstEach(local),
        sala: converters.CapitalizeFirstEach(sala),
        hora: hora,
        carga: carga,
        professor: converters.CapitalizeFirstEach(professor),
        dia_semana: dia_semana
      });
    }

    return {
      success: resp.status == 200,
      data: sendArray
    };
  }
};

module.exports = proxy_fns;
