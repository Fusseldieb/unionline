const axios = require("axios");
const querystring = require("querystring"); // To parse the POST properly so the dinossaur-age server can read it
const clog = require("../consoleLogger.js");
const reason_msg = require("../reasonMessages.js");
const converters = require("../converters.js");

async function getBoletos(session, matricula) {
  const resp = await axios({
    method: "POST",
    url: "/AOnline/financeiro/T215D.ajax",
    data: querystring.stringify({
      _id: "gridBoletos",
      _p_0: matricula
    }),
    headers: {
      Cookie: `JSESSIONID=${session.JSESSIONID};`,
      cronos_xsrf_token: session.XSRFToken
    }
  });
  return resp;
}

async function downloadBoleto(session, matricula, boleto_id) {
  const resp = await axios({
    responseType: "arraybuffer",
    // responseType: "blob",
    method: "POST",
    url: "/AOnline/financeiro/T215D.ajax",
    data: querystring.stringify({
      _id: "gpfDownload",
      _p_0: matricula,
      _p_1: boleto_id,
      _timestamp: null // Original: 1550910371973 - Probably not needed
    }),
    headers: {
      Cookie: `JSESSIONID=${session.JSESSIONID};`,
      cronos_xsrf_token: session.XSRFToken
    }
  });

  // const buffer = Buffer.from(new Uint8Array(resp.data));
  const buffer = Buffer.from(resp.data);
  return buffer;
}

const proxy_fns = {
  // The parameter "req" should contain the "req" element from Express,
  // containing the server response, session and other things.
  async get(req) {
    const resp = await getBoletos(req.session, req.query.matricula);

    // This function formats the received data
    // in order to only deliver the necessary informations
    // const formatted = format_data(resp.data);
    const recv = resp.data;
    if (typeof recv == "object") {
      var boletos = recv.data.records;
      var boletos2 = [];
      for (var i = 0; i < boletos.length; i++) {
        boletos2.push({
          boleto_id: boletos[i][0],
          vencimento: converters.SQLDateToTimestamp(boletos[i][1]),
          valor: boletos[i][2]
        });
      }

      clog("debug", "Formatter done processing successfully");
    } else {
      reason = reason_msg("formatter_type_mismatch");
    }

    return {
      success: resp.status == 200,
      data: boletos2
    };
  },

  async download(req) {
    const buffer = await downloadBoleto(req.session, req.query.matricula, req.query.boleto_id);
    // fs.writeFileSync("10111.pdf", buffer, "binary");
    return buffer;
  }
};

module.exports = proxy_fns;
