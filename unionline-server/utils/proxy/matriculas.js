const axios = require("axios");
const querystring = require("querystring"); // To parse the POST properly so the dinossaur-age server can read it
const clog = require("../../utils/consoleLogger.js");
const reason_msg = require("../../utils/reasonMessages.js");
const converters = require("../converters.js");

async function getMatriculas(session) {
  const resp = await axios({
    method: "POST",
    url: "/AOnline/avisos/T016D.ajax",
    data: querystring.stringify({
      _id: "blkEscolhaAluno.drpAluno"
    }),
    headers: {
      Cookie: `JSESSIONID=${session.JSESSIONID};`,
      cronos_xsrf_token: session.XSRFToken
    }
  });
  return resp;
}

const matriculas = {
  // The parameter "req" should contain the "req" element from Express,
  // containing the server response, session and other things.
  async get_own(req) {
    const resp = await getMatriculas(req.session);

    const matriculas = resp.data.data.records;
    let sendArray = [];
    for (var i = 0; i < matriculas.length; i++) {
      var splitmatr = matriculas[i][1].split(" - ");
      sendArray.push({
        matricula: splitmatr[0],
        aluno: splitmatr[1],
        curso: converters.CapitalizeFirstEach(splitmatr[2]),
        situacao: splitmatr[3]
      });
    }

    return {
      success: resp.status == 200,
      data: sendArray
    };
  }
};

module.exports = matriculas;
