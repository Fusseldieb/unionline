const axios = require("axios");
const querystring = require("querystring"); // To parse the POST properly so the dinossaur-age server can read it
const clog = require("../consoleLogger.js");
const reason_msg = require("../reasonMessages.js");
const converters = require("../converters.js");

const disciplinas = require("../proxy/disciplinas.js");

// TODO: Rewrite publicacoes

async function getPublicacoesPerID(session, disciplina_obj) {
  const resp = await axios({
    method: "POST",
    url: "/AOnline/disciplina/T019D.ajax",
    data: querystring.stringify({
      _id: "grdPublicacoesTurma",
      _p_0: disciplina_obj.disciplina_id,
      _p_1: null, // 0065-03A-TEORICA // Not necessary
      _p_2: disciplina_obj.ano_letivo,
      _p_3: disciplina_obj.semestre,
      _p_4: disciplina_obj.matricula
    }),
    headers: {
      Cookie: `JSESSIONID=${session.JSESSIONID};`,
      cronos_xsrf_token: session.XSRFToken
    }
  });
  return resp;
}

async function downloadPublicacao(session, file_id) {
  console.log("Download ID: " + file_id);
  const resp = await axios({
    responseType: "arraybuffer",
    // responseType: "blob",
    method: "POST",
    url: "/AOnline/disciplina/T019D.ajax",
    data: querystring.stringify({
      _id: "gpfArquivoTurma",
      _p_0: file_id,
      _timestamp: null // Original: 1550910371973 - Probably not needed
    }),
    headers: {
      Cookie: `JSESSIONID=${session.JSESSIONID};`,
      cronos_xsrf_token: session.XSRFToken
    }
  });

  // const buffer = Buffer.from(new Uint8Array(resp.data));
  const buffer = Buffer.from(resp.data);
  return buffer;
}

const publicacoes = {
  // The parameter "req" should contain the "req" element from Express,
  // containing the server response, session and other things.
  async get(req) {
    const resp = await disciplinas._getDisciplinas(
      req.session,
      req.query.matricula
    );

    const recv = resp.data;
    let sendArray = [];
    if (typeof recv == "object") {
      var discipl = recv.data.records;
      for (var i = 0; i < discipl.length; i++) {
        // Get all disciplinas
        //
        // Received from Lyceum:
        // =====================
        // {
        //   "data": {
        //     "records": [
        //       ["OPTATIVA", null, "SANDRA ELAINE AIRES ABREU", "03816", "EAD-0102-02A", "2019", "1", null],
        //       ["FUNDAMENTOS DE GESTÃO", null, "Ieso Costa Marques", "05091", "EAD-0018-02A", "2019", "1", null],
        //       ["MARKETING", null, null, "06106", "0065-03A", "2019", "1", "03/09/2019 00:00:00"],
        //       ["ILUSTRAÇÃO", null, "", "06976", "0065-03A-TEORICA", "2019", "1", "03/12/2019 00:00:00"],
        //       ["PRODUÇÃO PARA WEB", null, "NELSON VIEIRA MARTINS", "06977", "0065-03A-TEORICA", "2019", "1", "02/22/2019 00:00:00"],
        //       ["DESIGN DE SINALIZAÇÃO II", null, "LUCAS GABRIEL CORREA VARGAS", "06979", "0065-03A-TEORICA", "2019", "1", "03/16/2019 00:00:00"],
        //       ["MARCAS E IDENTIDADE VISUAL", null, "Allyson Barbosa da Silva", "06984", "0065-03A-TEORICA", "2019", "1", "02/22/2019 00:00:00"],
        //       ["SOFTWARE EM DESIGN III ", null, "NELSON VIEIRA MARTINS", "07549", "0065-03A", "2019", "1", "03/07/2019 00:00:00"],
        //       ["CIDADANIA, ÉTICA E ESPIRITUALIDADE", null, "Tiago Meireles Do Carmo Morais", "08379", "0065-03A", "2019", "1", "02/23/2019 00:00:00"]
        //     ],
        //     "count": 9
        //   },
        //   "commands": [{
        //     "jsCode": "show('grdDisciplina');"
        //   }, {
        //     "jsCode": "hide('htmErro');"
        //   }]
        // }
        const publ_res = await getPublicacoesPerID(req.session, {
          disciplina_id: discipl[i][7],
          ano_letivo: discipl[i][9],
          semestre: discipl[i][10],
          matricula: req.query.matricula
        });
        const publ_id_recv = publ_res.data.data.records;

        publ_id_recv.forEach(element => {

          // Get all posts from a disciplina
          //
          // Received from Lyceum:
          // =====================
          // {
          //   "data": {
          //     "records": [
          //       ["Aula 01", "Aula 1 - Pictogramas..pdf", null, "2371870", "03/02/2019 00:00:00", "338032"],
          //       ["Texto Base para a Aula 2.", "Caderno-de-Estudos-Avanc¸ados-em-Design-\u2013-Semio´tica.pdf", "Texto pag. 13 a 26.", "5272994", "03/02/2019 00:00:00", "338034"],
          //       ["Plano", "Plano - 2019.1 Design de Sinalização.pdf", null, "684337", "03/02/2019 00:00:00", "338033"],
          //       ["Folder", "26-01-2016_folder_fotoluminescente.pdf", null, "4412044", "03/16/2019 00:00:00", "340251"],
          //       ["Aula 02", "Aula 2 Acessibilidade, sinalização.pdf", null, "2439979", "03/16/2019 00:00:00", "340256"],
          //       ["NT 20", "nt-20_2014-sinalizacao-de-emergencia.pdf", null, "1348559", "03/16/2019 00:00:00", "340252"],
          //       ["Sinalização", "Sinalização.pdf", null, "1578589", "03/16/2019 00:00:00", "340250"]
          //     ],
          //     "count": 7
          //   },
          //   "commands": [{
          //     "jsCode": "show('grdPublicacoesTurma');"
          //   }, {
          //     "jsCode": "hide('htmErroTurma');"
          //   }]
          // }
          sendArray.push({
            nome_arquivo: converters.CapitalizeFirstEach(element[0]),
            nome_arquivo_raw: element[1],
            data: converters.SQLDateToTimestamp(element[4]),
            descricao: element[2],
            arquivo_id: element[5],
            disciplina_id: discipl[i][7],
            disciplina: discipl[i][0].split(" - ")[1]
          });
        });
      }

      reason = reason_msg("request_general_ok");

      clog("debug", "Formatter done processing successfully");
    } else {
      console.log(resp.data);
      reason_msg("formatter_type_mismatch");
    }

    return {
      success: resp.status == 200,
      data: sendArray
    };
  },
  async download(req) {
    // console.log("DOWNLOAD");
    const buffer = await downloadPublicacao(req.session, req.query.file_id);
    // fs.writeFileSync("10111.pdf", buffer, "binary");.
    return buffer;
  }
};

module.exports = publicacoes;
