const axios = require("axios");
const querystring = require("querystring"); // To parse the POST properly so the dinossaur-age server can read it
const clog = require("../consoleLogger.js");
const reason_msg = require("../reasonMessages.js");
const converters = require("../converters.js");

const proxy_fns = {
  async _getDisciplinas(session, matricula) {
    const resp = await axios({
      method: "POST",
      url: "/AOnline/disciplina/T020D.ajax",
      data: querystring.stringify({
        _id: "grdDisciplinaMatriculadas",
        _p_0: matricula
      }),
      headers: {
        Cookie: `JSESSIONID=${session.JSESSIONID};`,
        cronos_xsrf_token: session.XSRFToken
      }
    });
    return resp;
  },
  // The parameter "req" should contain the "req" element from Express,
  // containing the server response, session and other things.
  async get(req) {
    const resp = await this._getDisciplinas(req.session, req.query.matricula);

    // This function formats the received data
    // in order to only deliver the necessary informations
    //const formatted = format_data(resp.data);

    const recv = resp.data;

    var disciplinas = recv.data.records;
    var disciplinas2 = [];
    for (var i = 0; i < disciplinas.length; i++) {
      var splitdisc = disciplinas[i][0].split(" - ");

      // Received from Lyceum:
      // =====================
      // {
      //   "data": {
      //     "records": [
      //       ["03816 - OPTATIVA", null, "Matriculado", "1ºSEMESTRE DE 2019", "SANDRA ELAINE AIRES ABREU", null, "01/28/2019 00:00:00", "03816", "06/30/2019 00:00:00", "2019", "1", "<img src=\"../../image/ico/btDown.png\" title=\"Ementa\" />", "<img src=\"../../image/ico/btDiario.png\" title=\"Diário de Classe\" />", "<img src=\"../../image/ico/btPubli.png\" title=\"Publicações\" />", "<img src=\"../../image/ico/btPlan.png\" title=\"Plano Didático Pedagógico\" />", "<img src=\"../../image/ico/btCalc.png\" title=\"Fórmulas e Critérios de Avaliação\" />", "EAD-0102-02A", "910"],
      //       ["05091 - FUNDAMENTOS DE GESTÃO", null, "Matriculado", "1ºSEMESTRE DE 2019", "Ieso Costa Marques", null, "01/28/2019 00:00:00", "05091", "06/30/2019 00:00:00", "2019", "1", "<img src=\"../../image/ico/btDown.png\" title=\"Ementa\" />", "<img src=\"../../image/ico/btDiario.png\" title=\"Diário de Classe\" />", "<img src=\"../../image/ico/btPubli.png\" title=\"Publicações\" />", "<img src=\"../../image/ico/btPlan.png\" title=\"Plano Didático Pedagógico\" />", "<img src=\"../../image/ico/btCalc.png\" title=\"Fórmulas e Critérios de Avaliação\" />", "EAD-0018-02A", "2977"],
      //       ["06106 - MARKETING", null, "Matriculado", "1ºSEMESTRE DE 2019", null, null, "01/28/2019 00:00:00", "06106", "06/30/2019 00:00:00", "2019", "1", "<img src=\"../../image/ico/btDown.png\" title=\"Ementa\" />", "<img src=\"../../image/ico/btDiario.png\" title=\"Diário de Classe\" />", "<img src=\"../../image/ico/btPubli.png\" title=\"Publicações\" />", "<img src=\"../../image/ico/btPlan.png\" title=\"Plano Didático Pedagógico\" />", "<img src=\"../../image/ico/btCalc.png\" title=\"Fórmulas e Critérios de Avaliação\" />", "0065-03A", "3336"],
      //       ["06976 - ILUSTRAÇÃO", null, "Matriculado", "1ºSEMESTRE DE 2019", "", null, "01/28/2019 00:00:00", "06976", "06/30/2019 00:00:00", "2019", "1", "<img src=\"../../image/ico/btDown.png\" title=\"Ementa\" />", "<img src=\"../../image/ico/btDiario.png\" title=\"Diário de Classe\" />", "<img src=\"../../image/ico/btPubli.png\" title=\"Publicações\" />", "<img src=\"../../image/ico/btPlan.png\" title=\"Plano Didático Pedagógico\" />", "<img src=\"../../image/ico/btCalc.png\" title=\"Fórmulas e Critérios de Avaliação\" />", "0065-03A-TEORICA", "6476"],
      //       ["06977 - PRODUÇÃO PARA WEB", null, "Matriculado", "1ºSEMESTRE DE 2019", "NELSON VIEIRA MARTINS", null, "01/28/2019 00:00:00", "06977", "06/30/2019 00:00:00", "2019", "1", "<img src=\"../../image/ico/btDown.png\" title=\"Ementa\" />", "<img src=\"../../image/ico/btDiario.png\" title=\"Diário de Classe\" />", "<img src=\"../../image/ico/btPubli.png\" title=\"Publicações\" />", "<img src=\"../../image/ico/btPlan.png\" title=\"Plano Didático Pedagógico\" />", "<img src=\"../../image/ico/btCalc.png\" title=\"Fórmulas e Critérios de Avaliação\" />", "0065-03A-TEORICA", "5075"],
      //       ["06979 - DESIGN DE SINALIZAÇÃO II", null, "Matriculado", "1ºSEMESTRE DE 2019", "LUCAS GABRIEL CORREA VARGAS", null, "01/28/2019 00:00:00", "06979", "06/30/2019 00:00:00", "2019", "1", "<img src=\"../../image/ico/btDown.png\" title=\"Ementa\" />", "<img src=\"../../image/ico/btDiario.png\" title=\"Diário de Classe\" />", "<img src=\"../../image/ico/btPubli.png\" title=\"Publicações\" />", "<img src=\"../../image/ico/btPlan.png\" title=\"Plano Didático Pedagógico\" />", "<img src=\"../../image/ico/btCalc.png\" title=\"Fórmulas e Critérios de Avaliação\" />", "0065-03A-TEORICA", "771"],
      //       ["06984 - MARCAS E IDENTIDADE VISUAL", null, "Matriculado", "1ºSEMESTRE DE 2019", "Allyson Barbosa da Silva", null, "01/28/2019 00:00:00", "06984", "06/30/2019 00:00:00", "2019", "1", "<img src=\"../../image/ico/btDown.png\" title=\"Ementa\" />", "<img src=\"../../image/ico/btDiario.png\" title=\"Diário de Classe\" />", "<img src=\"../../image/ico/btPubli.png\" title=\"Publicações\" />", "<img src=\"../../image/ico/btPlan.png\" title=\"Plano Didático Pedagógico\" />", "<img src=\"../../image/ico/btCalc.png\" title=\"Fórmulas e Critérios de Avaliação\" />", "0065-03A-TEORICA", "3209"],
      //       ["07549 - SOFTWARE EM DESIGN III ", null, "Matriculado", "1ºSEMESTRE DE 2019", "NELSON VIEIRA MARTINS", null, "01/28/2019 00:00:00", "07549", "06/30/2019 00:00:00", "2019", "1", "<img src=\"../../image/ico/btDown.png\" title=\"Ementa\" />", "<img src=\"../../image/ico/btDiario.png\" title=\"Diário de Classe\" />", "<img src=\"../../image/ico/btPubli.png\" title=\"Publicações\" />", "<img src=\"../../image/ico/btPlan.png\" title=\"Plano Didático Pedagógico\" />", "<img src=\"../../image/ico/btCalc.png\" title=\"Fórmulas e Critérios de Avaliação\" />", "0065-03A", "5075"],
      //       ["08379 - CIDADANIA, ÉTICA E ESPIRITUALIDADE", null, "Matriculado", "1ºSEMESTRE DE 2019", "Tiago Meireles Do Carmo Morais", null, "01/28/2019 00:00:00", "08379", "06/30/2019 00:00:00", "2019", "1", "<img src=\"../../image/ico/btDown.png\" title=\"Ementa\" />", "<img src=\"../../image/ico/btDiario.png\" title=\"Diário de Classe\" />", "<img src=\"../../image/ico/btPubli.png\" title=\"Publicações\" />", "<img src=\"../../image/ico/btPlan.png\" title=\"Plano Didático Pedagógico\" />", "<img src=\"../../image/ico/btCalc.png\" title=\"Fórmulas e Critérios de Avaliação\" />", "0065-03A", "4514"]
      //     ],
      //     "count": 9
      //   },
      //   "commands": [{
      //     "jsCode": "show('grdDisciplinaMatriculadas');"
      //   }, {
      //     "jsCode": "hide('htmErroDisciplinas');"
      //   }]
      // }

      disciplinas2.push({
        disciplina_id: splitdisc[7],
        disciplina: converters.CapitalizeFirstEach(splitdisc[1]),
        situacao: disciplinas[i][2],
        periodo: disciplinas[i][3],
        professor: converters.CapitalizeFirstEach(disciplinas[i][4]),
        ano_letivo: disciplinas[i][9],
        semestre: disciplinas[i][10]
      });
    }

    return {
      success: resp.status == 200,
      data: disciplinas2
    };
  }
};

module.exports = proxy_fns;
