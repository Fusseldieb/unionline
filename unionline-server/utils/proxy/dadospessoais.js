const axios = require("axios");
const querystring = require("querystring"); // To parse the POST properly so the dinossaur-age server can read it
const clog = require("../consoleLogger.js");
const reason_msg = require("../reasonMessages.js");
const converters = require("../converters.js");

async function getDadosPessoais(session) {
  const resp = await axios({
    method: "POST",
    url: "/AOnline/cadastro/T004D.ajax",
    data: querystring.stringify({
      _id: "rcpDadosPessoais"
    }),
    headers: {
      Cookie: `JSESSIONID=${session.JSESSIONID};`,
      cronos_xsrf_token: session.XSRFToken
    }
  });
  return resp;
}

const proxy_fns = {
  // This function is used to log the user in
  // The parameter "req" should contain the "req" element from Express,
  // containing the server response, session and other things.
  async get_own(req) {
    const resp = await getDadosPessoais(req.session);

    // This function formats the received data
    // in order to only deliver the necessary informations
    // const formatted = format_data(resp.data);

    const recv = resp.data;

    if (typeof recv == "object") {
      var recvArray = recv.data.records;
      var sendObj = [];
      for (let index = 0; index < recvArray.length; index++) {
        const aluno_id = recvArray[index][0];
        const nome = recvArray[index][1];
        const nome_social = recvArray[index][2];
        const pai = recvArray[index][3];
        const naturalidade = recvArray[index][4];
        const sexo = recvArray[index][5];
        const estado_civil = recvArray[index][6];
        const religiao = recvArray[index][7];

        const nome_abreviado = recvArray[index][8];
        const data_nascimento = recvArray[index][9];
        const mae = recvArray[index][10];
        const nacionalidade = recvArray[index][11];
        const cor_pele = recvArray[index][12];
        const necessidade_especial = recvArray[index][13];

        const cep = recvArray[index][14];
        const numero = recvArray[index][15];
        const bairro = recvArray[index][16];
        const uf = recvArray[index][17];
        const tel_residencial = recvArray[index][18];
        const email = recvArray[index][19]; // TODO: Add lowerStr

        const endereco = recvArray[index][20];
        const complemento = recvArray[index][21];
        const cidade = recvArray[index][22];
        const pais = recvArray[index][23];
        const tel_celular = recvArray[index][24];
        const envio_email_sms = recvArray[index][25] == "Sim";

        const cep_comercial = recvArray[index][26];
        const cargo = recvArray[index][27];
        const email_comercial = recvArray[index][28]; // TODO: Add lowerStr
        const numero_comercial = recvArray[index][29];
        const bairro_comercial = recvArray[index][30];
        const estado_comercial = recvArray[index][31];

        const empresa = recvArray[index][32];
        const profissao = recvArray[index][33];
        const endereco_comercial = recvArray[index][34];
        const complemento_comercial = recvArray[index][35];
        const cidade_comercial = recvArray[index][36];
        const pais_comercial = recvArray[index][37];

        const rg = recvArray[index][38];
        const data_expedicao_rg = recvArray[index][39];
        const titulo_eleitor = recvArray[index][40];
        const secao_eleitor = recvArray[index][41];
        const categoria_cert_reservista = recvArray[index][42];
        const csm_cert_reservista = recvArray[index][43];
        const folha_cert_nascimento = recvArray[index][44];
        const data_expedicao_cert_reservista = recvArray[index][45];
        const cartorio_expeditor_cert_nascimento = recvArray[index][46];

        const orgao_expeditor_rg = recvArray[index][47];
        const cpf = recvArray[index][48];
        const zona_titulo_eleitor = recvArray[index][49];
        const numero_cert_reservista = recvArray[index][50];
        const serie_cert_reservista = recvArray[index][51];
        const rm_cert_reservista = recvArray[index][52];
        const livro_cert_nascimento = recvArray[index][53];
        const numero_cert_nascimento = recvArray[index][54];
        const uf_cartorio_cert_nascimento = recvArray[index][55];

        sendObj = {
          aluno_id: aluno_id,
          nome: converters.CapitalizeFirstEach(nome),
          nome_social: converters.CapitalizeFirstEach(nome_social),
          pai: converters.CapitalizeFirstEach(pai),
          naturalidade: converters.CapitalizeFirstEach(naturalidade),
          sexo: converters.CapitalizeFirstEach(sexo),
          estado_civil: converters.CapitalizeFirstEach(estado_civil),
          religiao: converters.CapitalizeFirstEach(religiao),
          nome_abreviado: converters.CapitalizeFirstEach(nome_abreviado),
          data_nascimento: data_nascimento,
          mae: converters.CapitalizeFirstEach(mae),
          nacionalidade: converters.CapitalizeFirstEach(nacionalidade),
          cor_pele: converters.CapitalizeFirstEach(cor_pele),
          necessidade_especial: converters.CapitalizeFirstEach(necessidade_especial),
          cep: cep,
          numero: numero,
          bairro: converters.CapitalizeFirstEach(bairro),
          uf: converters.CapitalizeFirstEach(uf),
          tel_residencial: tel_residencial,
          email: converters.AllLowerCase(email),
          endereco: converters.CapitalizeFirstEach(endereco),
          complemento: converters.CapitalizeFirstEach(complemento),
          cidade: converters.CapitalizeFirstEach(cidade),
          pais: converters.CapitalizeFirstEach(pais),
          tel_celular: tel_celular,
          envio_email_sms: envio_email_sms,
          cep_comercial: cep_comercial,
          cargo: converters.CapitalizeFirstEach(cargo),
          email_comercial: converters.AllLowerCase(email_comercial),
          numero_comercial: numero_comercial,
          bairro_comercial: converters.CapitalizeFirstEach(bairro_comercial),
          estado_comercial: converters.CapitalizeFirstEach(estado_comercial),
          empresa: converters.CapitalizeFirstEach(empresa),
          profissao: converters.CapitalizeFirstEach(profissao),
          endereco_comercial: converters.CapitalizeFirstEach(endereco_comercial),
          complemento_comercial: converters.CapitalizeFirstEach(complemento_comercial),
          cidade_comercial: converters.CapitalizeFirstEach(cidade_comercial),
          pais_comercial: converters.CapitalizeFirstEach(pais_comercial),
          rg: rg,
          data_expedicao_rg: data_expedicao_rg,
          titulo_eleitor: titulo_eleitor,
          secao_eleitor: secao_eleitor,
          categoria_cert_reservista: categoria_cert_reservista,
          csm_cert_reservista: csm_cert_reservista,
          folha_cert_nascimento: folha_cert_nascimento,
          data_expedicao_cert_reservista: data_expedicao_cert_reservista,
          cartorio_expeditor_cert_nascimento: cartorio_expeditor_cert_nascimento,
          orgao_expeditor_rg: orgao_expeditor_rg,
          cpf: cpf,
          zona_titulo_eleitor: zona_titulo_eleitor,
          numero_cert_reservista: numero_cert_reservista,
          serie_cert_reservista: serie_cert_reservista,
          rm_cert_reservista: rm_cert_reservista,
          livro_cert_nascimento: livro_cert_nascimento,
          numero_cert_nascimento: numero_cert_nascimento,
          uf_cartorio_cert_nascimento: uf_cartorio_cert_nascimento
        };
      }
      clog("debug", "Formatter done processing successfully");
    } else {
      reason = reason_msg("formatter_type_mismatch");
    }

    return {
      success: resp.status == 200,
      data: sendObj
    };
  }
};

module.exports = proxy_fns;
