const axios = require("axios");
const querystring = require("querystring"); // To parse the POST properly so the dinossaur-age server can read it
const clog = require("../../utils/consoleLogger.js");
const ecm = require("../reasonMessages.js");

const proxy_fns = {
  // This function is used to log the user in
  // The parameter "req" should contain the "req" element from Express,
  // containing the server response, session and other things.
  async authenticate(req) {
    const resp = await axios({
      method: "POST",
      url: "/web/loginsm",
      data: querystring.stringify({
        username: req.body.username,
        password: req.body.password
      }),
      // headers: {
        // Cookie: `JSESSIONID=${req.session.JSESSIONID};` // Send the server-side saved cookie back to the server
      // }
    });

    // This variable contains the failure reason as a String, if it exists
    let reason;

    switch (resp.status) {
      case 200:
        // Pick only the JSESSIONID from the String using Regex
        // resp.headers["set-cookie"][0] contains the Cookie String
        // With match(jsession_regex)[1] we match and use the first Regex group
        const jsession_regex = /JSESSIONID=(.*?);/;
        const regex_res = resp.headers["set-cookie"][0].match(jsession_regex);
        const JSESSIONID = regex_res[1];
        clog("debug", "JSESSIONID obtained: " + JSESSIONID);
        req.session.JSESSIONID = JSESSIONID; // Save it server-side

        // console.log(resp)

        const resp2 = await axios({
          method: "GET",
          url: "/XSRFScript",
          headers: {
            Cookie: `JSESSIONID=${req.session.JSESSIONID};`, // Send the server-side saved cookie back to the server
            Referer:
              "https://academico.unievangelica.edu.br/AOnline/AOnline/avisos/T016D.tp"
          }
        });

        const xsrf_token_regex = /cronos_xsrf_token="(.*?)"/g;
        var match = xsrf_token_regex.exec(resp2.data);
        console.log("Got XSRF Token:" + match[1]);
        req.session.XSRFToken = match[1];

        reason = ecm("auth_ok");
        break;

      case 500:
        // Error 500 means that the user is already logged
        // or that the user/password combination isn't correct
        // TODO: Make something to differentiate the two
        reason = ecm("already_logged_or_incorrect_credentials");
      default:
        reason = ecm("unknown");
    }

    return {
      success: resp.status == 200,
      reason: reason
    };
  },
  async deauthenticate(req) {
    const resp = await axios({
      method: "GET",
      url: "/logoff.jsp",
      data: {
        // "unievangelica" in Base64. WTF?
        // Taken from the official Lyceum request - not sure if needed
        t: "dW5pZXZhbmdlbGljYQ=="
      },
      headers: {
        Cookie: `JSESSIONID=${req.session.JSESSIONID};` // Send the server-side saved cookie back to the server
      }
    });

    let reason;

    switch (resp.status) {
      case 401:
        reason = ecm("deauth_ok");
        break;

      default:
        reason = ecm("unknown");
        break;
    }

    return {
      success: resp.status == 401,
      reason: reason
    };
  }
};

module.exports = proxy_fns;
