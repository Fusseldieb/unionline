require("colors");

const log = function(type, message) {
  // This function allows us to use simplified arguments in log()
  // but generate messages which are colored and prefixed
  // depending on the type (error,warning,normal)
  // It accepts both Strings and Objects as "message".
  let prepend_msg = "";
  switch (type) {
    case "error":
      prepend_msg = "[LRP-ERR] ".red;
      break;
    case "warning":
      prepend_msg = "[LRP-WRN] ".yellow;
      break;
    case "debug":
      prepend_msg = "[LRP-DBG] ".gray;
      break;
  }

  // Check which message type
  // Depending, we must split it into multiple prints.
  switch (typeof message) {
    case "object":
      let join_str;
      for (const [key, value] of Object.entries(message)) {
        console.log(prepend_msg + key + "       " + value);
      }
      message = join_str;
      break;
    default:
      console.log(prepend_msg + message);
      break;
  }
};

module.exports = log;
