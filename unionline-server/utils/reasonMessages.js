const clog = require("./consoleLogger.js");

// This object holds all possible errors and gives humanly readable
// logs out in the console, coloring them depending on the error_level

// "error_level" can be following:
// "debug" - Debug log, no problem
// "warning" - Warning log, could possibly be a problem
// "error" - Error log, represents a problem and should never happen
// If none set, it will fallback to "debug" with a warning
const causesObj = {
  already_logged_or_incorrect_credentials: {
    message: "User already logged in or incorrect credentials",
    error_level: "debug"
  },
  auth_ok: {
    message: "Successfully authenticated",
    error_level: "debug"
  },
  deauth_ok: {
    message: "Successfully deauthenticated",
    error_level: "debug"
  },
  request_general_ok: {
    message: "The request was completed successfully",
    error_level: "debug"
  },
  unknown: {
    message: "A unknown error occurred",
    error_level: "warning"
  },
  request_malformed: {
    message: "The server didn't understood the request",
    error_level: "warning"
  },
  not_logged_in: {
    message: "The user is not logged in to perform that request",
    error_level: "warning"
  },
  formatter_type_mismatch: {
    message: "Formatter expected Object, got String. Could not format.",
    error_level: "error"
  }
};

const ecm = function(cause) {
  const error_obj = causesObj[cause];
  if (error_obj) {
    if (!error_obj.error_level) {
      clog("warning", `Error Level for '${cause}' not set`);
    }

    clog(
      error_obj.error_level ? error_obj.error_level : "debug",
      error_obj.message
    );
  } else {
    clog("error", `Error Message for '${cause}' not found`);
  }

  return cause;
};

module.exports = ecm;
