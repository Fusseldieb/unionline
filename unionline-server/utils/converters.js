const converters = {
  SQLDateToTimestamp(sqldate) {
    return Date.parse(sqldate);
  },
  DateToTimestamp(date) {
    const date_splitted = date.split("/");
    return new Date(date_splitted[1] + "/" + date_splitted[0] + "/" + date_splitted[2]).getTime();
  },
  CapitalizeFirstEach(string) {
    if (!string) return "";
    // Reference: https://stackoverflow.com/a/4878800/3525780
    return string
      .toLowerCase()
      .split(" ")
      .map(s => s.charAt(0).toUpperCase() + s.substring(1))
      .join(" ");
  },
  AllLowerCase(string) {
    if (!string) return "";
    return string.toLowerCase();
  },
  AllUpperCase(string) {
    if (!string) return "";
    return string.toUpperCase();
  },
  RemoveStarAtEnd(string) {
    if (!string) return "";
    return string.replace(/\*\s*$/, "");
  }
};

module.exports = converters;
