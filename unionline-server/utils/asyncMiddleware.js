const clog = require("../utils/consoleLogger");

// This Middleware allows us to use "async/await" within Express
// It essentially catches Exceptions/Rejects from all routes
// This allows us to reduce duplicate code
const asyncMiddleware = fn => (req, res, next) => {
  Promise.resolve()
    .then(() => fn(req, res, next))
    .catch(err => {
      clog("error", "Promise was rejected due to following error:");
      clog("error", err.toString());

      if (err.toString().indexOf("timeout") > -1) {
        res.json({
          success: false,
          reason: "lyceum_server_timeout"
        });
      } else {
        res.json({
          success: false,
          reason: "server_promise_rejection"
        });
      }
    });
};

module.exports = asyncMiddleware;
