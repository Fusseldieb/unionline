const nodeExternals = require("webpack-node-externals");
const CopyPlugin = require("copy-webpack-plugin");

module.exports = [
  {
    name: "server",
    entry: "./app.js",
    output: {
      path: __dirname + "/../dist",
      filename: "bundle.js"
    },
    mode: "production",
    target: "node", // in order to ignore built-in modules like path, fs, etc.
    externals: [
      // nodeExternals(),
      nodeExternals({
        // modulesDir: path.resolve(__dirname + "/../node_modules")
      })
    ],
    plugins: [
      new CopyPlugin([
        { from: "../unionline-frontend/dist", to: "../dist/frontend" }
      ])
    ]
  }
];
