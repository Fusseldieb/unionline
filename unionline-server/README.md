# UniOnline - Facilitando sua vida EAD

Este é o código fonte do UniOnline, contendo todos os arquivos necessários para rodar a *API/Proxy* do UniOnline.

Este servidor é capaz de rodar em modo Standalone, ou seja, não necessita de outro componente rodando junto pra funcionar.

Todas as contribuições são bem-vindas, porém comente o código em *inglês* e documente Wikis ou `README.md`s em *português*.

#### Para baixar todas as dependências necessárias, digite:
```
npm install
```

#### Com o comando abaixo, você executará a API/Proxy do UniOnline
(No modo de desenvolvedor, alterações em arquivos farão a API/Proxy reiniciar com as novas alterações.)
```
npm run serve
```
