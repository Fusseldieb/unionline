const express = require("express"); // Webserver
const app = express();
const bodyParser = require("body-parser"); // Used to process received POST's (req.body)
const cors = require("cors"); // Used for debugging (Running API and Frontend on separate ports)
const axios = require("axios");
const session = require("express-session");
const clog = require("./utils/consoleLogger.js");

// If the argument "serve" is passed, then make it available on port 5000, instead of 3000 (Umbler port).
// And other things
const debug_serve = process.argv.slice(2)[0] == "serve";

app.use(
  bodyParser.urlencoded({
    extended: false
  })
);
app.use(bodyParser.json());
app.use(cors());
app.use(
  session({
    secret: "lyrp-opensource",
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false }
  })
);

axios.defaults.baseURL = "https://academico.unievangelica.edu.br/AOnline/";
axios.defaults.timeout = 10000;
axios.defaults.withCredentials = true;
axios.defaults.headers = {
  "Content-Type": "application/x-www-form-urlencoded",
  "X-Requested-With": "XMLHttpRequest",
  Connection: "keep-alive",
  "User-Agent": "UniOnline/0.0.3prealpha",
  "X-Message": "Lyceum (Revival Project) > 'UniOnline' #UniEvangelica"
};
axios.defaults.validateStatus = function(status) {
  clog("debug", `HTTP Status code ${status}`);
  // Always accept the promise, regardless of the HTTP status code
  // For instance, Lyceum uses error 401 to indicate a successfull logout (WTF?)
  // 401 is unauthorized, which would give a promise reject
  return true;
};

axios.interceptors.response.use(
  res => {
    console.log(res.request._header);
    return res;
  },
  error => Promise.reject(error)
);

app.get("*", (req, res, next) => {
  if (req.headers["x-forwarded-proto"] != "https" && !debug_serve) {
    console.log("Force HTTPS");
    res.redirect("https://" + req.headers.host + req.url);
  } else {
    next();
  }
});

app.use("/api", require("./controllers"));

if (!debug_serve) app.use(express.static("frontend"));

const port = debug_serve ? 5000 : 3000;
app.listen(port, () => {
  console.log(`UniOnline API/Proxy listening on port ${port}!`);
  // if (frontend_serve)
  console.log(`UniOnline Frontend listening on port ${port} too!`);
});
