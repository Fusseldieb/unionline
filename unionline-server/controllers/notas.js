const express = require("express");
const router = express.Router();
const asyncMw = require("../utils/asyncMiddleware.js");
const proxy_fn = require("../utils/proxy/notas.js");

router.get(
  "/",
  asyncMw(async (req, res) => {
    const response = await proxy_fn.get(req);

    // The const "response" contains a object ready for sending
    res.json(response);
  })
);

module.exports = router;
