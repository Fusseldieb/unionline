const express = require("express");
const router = express.Router();
const asyncMw = require("../utils/asyncMiddleware.js");
const proxy_fn = require("../utils/proxy/avisos.js");

router.get(
  "/get",
  asyncMw(async (req, res) => {
    // Uses the auth util to deauthenticate the user (Logout)
    // The session cookie will also be internally deleted
    const response = await proxy_fn.get(req);

    // The const "response" contains a object ready for sending
    // It consists of "success","data" and "reason"
    res.json(response);
  })
);

router.get(
  "/download",
  asyncMw(async (req, res) => {
    // Uses the auth util to deauthenticate the user (Logout)
    // The session cookie will also be internally deleted
    const response = await proxy_fn.download(req);

    // The const "response" contains a object ready for sending
    // It consists of "success","data" and "reason"
    res.end(response);
  })
);

module.exports = router;
