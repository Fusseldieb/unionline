const express = require("express");
const router = express.Router();
const asyncMw = require("../utils/asyncMiddleware.js");
const proxy_fn = require("../utils/proxy/horarios.js");

router.get(
  "/",
  asyncMw(async (req, res) => {
    const response = await proxy_fn.get(req);
    res.json(response);
  })
);

module.exports = router;
