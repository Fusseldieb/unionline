const express = require("express");
const router = express.Router();
const asyncMw = require("../utils/asyncMiddleware.js");
const proxy_fn = require("../utils/proxy/boletos.js");

router.get(
  "/get",
  asyncMw(async (req, res) => {
    const response = await proxy_fn.get(req);
    res.json(response);
  })
);

router.get(
  "/download",
  asyncMw(async (req, res) => {
    const response = await proxy_fn.download(req);
    res.end(response);
  })
);

module.exports = router;
