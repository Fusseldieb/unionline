const express = require("express");
const router = express.Router();

// Here we load all modules we want to expose in our API
// specifying the URL path also
router.use("/authentication", require("./authentication.js"));
router.use("/matriculas", require("./matriculas.js"));
router.use("/disciplinas", require("./disciplinas.js"));
router.use("/boletos", require("./boletos.js"));
router.use("/horarios", require("./horarios.js")); 
router.use("/faltas", require("./faltas.js"));
router.use("/dadospessoais", require("./dadospessoais.js")); 
router.use("/avisos", require("./avisos.js"));
router.use("/publicacoes", require("./publicacoes.js"));
router.use("/notas", require("./notas.js"));
router.use("/keepalive", require("./keepalive.js"));

router.get("/", function(req, res) {
  res.send({
    title: "UniOnline API",
    version: "0.0.2"
  });
});

module.exports = router;
