const express = require("express");
const router = express.Router();
const asyncMw = require("../utils/asyncMiddleware.js");
const proxy_fn = require("../utils/proxy/authentication.js");

// FIXME: Check if JSESSIONID is defined, if yes, the user is
// _probaly_ logged in. But what if the JSESSIONID expires on the other server?
// There's no easy way to know. We must access some URL that only load
// when logged in to check this.

router.post(
  "/login",
  asyncMw(async (req, res) => {
    // Uses the auth util to authenticate the user
    // The session cookie will be internally stored and re-sent
    // with every request
    // The entire "req" will be passed, since it needs "req.body" and "req.session"
    const response = await proxy_fn.authenticate(req);
    // The const "response" contains a object ready for sending
    res.json(response);
  })
);

// When logged out, it returns error 401, meaning successfully logout (WTF?)
router.get(
  "/logout",
  asyncMw(async (req, res) => {
    // Uses the auth util to deauthenticate the user (Logout)
    // The session cookie will also be internally deleted
    const response = await proxy_fn.deauthenticate(req);

    req.session.JSESSIONID = undefined;

    // The const "response" contains a object ready for sending
    res.json(response);
  })
);

module.exports = router;
