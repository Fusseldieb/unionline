const express = require("express");
const router = express.Router();
const asyncMw = require("../utils/asyncMiddleware.js");
const matr = require("../utils/proxy/matriculas.js");

router.get(
  "/",
  asyncMw(async (req, res) => {
    const response = await matr.get_own(req);

    // The const "response" contains a object ready for sending
    res.json(response);
  })
);

module.exports = router;
